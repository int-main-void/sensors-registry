package com.greatsagemonkey.sensors.dataserver;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class SimpleDataServerMain {

	private final static Logger LOGGER = Logger.getLogger(SimpleDataServerMain.class.getName());
	private static final int serverPortNum = 1976;

	
	public static void main(String[] args) throws Exception {

		// TODO - setup - parse cmd line args and/or read configs
		
		int threadPoolSize = 1;
		ExecutorService es = Executors.newFixedThreadPool(threadPoolSize);

		ServerSocket s = null;
		s = new ServerSocket(serverPortNum);
		LOGGER.info("Listening on port " + serverPortNum);

		for(int i=0; i< threadPoolSize; i++) {
			es.submit(new Server(s));
		}
		
	}

}
