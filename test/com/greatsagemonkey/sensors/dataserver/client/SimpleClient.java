package com.greatsagemonkey.sensors.dataserver.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Logger;

import com.greatsagemonkey.sensors.dataserver.Command;

public class SimpleClient {

	private final static Logger LOGGER = Logger.getLogger(SimpleClient.class.getName());

	private static final String hostname = "localhost";
	private static final int port = 1976;

	public static void main(String[] args) throws Exception {


		BufferedReader inFromUser = new BufferedReader( new InputStreamReader(System.in));

		Socket clientSocket = new Socket(hostname, port);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
		BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

		try {
			String cmdStr = inFromUser.readLine();
			Command cmd = new Command(cmdStr);
			bw.write(cmd.toString());
			bw.newLine();
			bw.flush();

			LOGGER.info("FROM SERVER: ");
			String line;
			while( ( line = br.readLine()) != null) {
				System.out.println(line);
			}
		} finally {
			clientSocket.close();
		}

	}

}
