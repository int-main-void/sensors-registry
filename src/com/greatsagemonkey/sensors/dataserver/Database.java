/**
 * 
 */
package com.greatsagemonkey.sensors.dataserver;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/*
 *  TODO - create single db thread and/or connection pool
 */
public class Database {

	private final static Logger LOGGER = Logger.getLogger(Database.class.getName());
	
	Connection c = null;

	String driverName = "org.sqlite.JDBC";
	String dbName = "jdbc:sqlite:test.db";
	
	Database()
	{
		try {
			createConnection();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void createConnection() throws ClassNotFoundException, SQLException
	{
		Class.forName(driverName);
		LOGGER.info("opening database " + dbName);
		c = DriverManager.getConnection(dbName);
		c.setAutoCommit(false);
	}

	public void setValue(String key, String value) throws SQLException
	{
		String existingValue = getValue(key);
		if(existingValue == null) {
			insertValue(key, value);
		} else {
			updateValue(key, value);
		}
	}

	private void insertValue(String key, String value) throws SQLException {
		final String sql = "INSERT INTO registry (id, key, value) VALUES (?, ?, ?);"; 
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setNull(1, 0);
		stmt.setString(2, key);
		stmt.setString(3, value);
		stmt.addBatch();
		stmt.executeBatch();
		stmt.close();
		c.commit();
	}

	private void updateValue(String key, String value) throws SQLException {
		final String sql = "UPDATE registry SET value = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, value);
		stmt.executeUpdate(sql);
		stmt.close();
		c.commit();
	}

	public String getValue(String key) throws SQLException
	{
		LOGGER.info("Getting value for key: " + key);
		final String sql = "SELECT value FROM registry WHERE key = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setString(1, key);
		ResultSet rs = stmt.executeQuery();
		String value = null;
		while ( rs.next() ) {
			value = rs.getString("value");
		}
		rs.close();
		stmt.close();
		return value;
	}

	public Map<String,String> getAllValues() throws SQLException
	{
		final String sql = "SELECT id, key, value FROM registry ";
		HashMap<String,String> kvs = new HashMap<String,String>();
		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery( sql);
		while ( rs.next() ) {
			int id = rs.getInt("id");
			String key = rs.getString("key");
			String value = rs.getString("value");
			System.out.println("id: " + id + " key: " + key + " value: " + value);
			kvs.put(key, value);
		}
		rs.close();
		stmt.close();
		return kvs;
	}


}
