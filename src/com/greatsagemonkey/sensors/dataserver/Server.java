package com.greatsagemonkey.sensors.dataserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 *  implements the server side of a simple, line-based data store
 *  
 *  expects input from the socket in the form COMMAND?KEY1=VAL1,KEY2=VAL2
 *  
 */
public class Server implements Runnable {

	private final static Logger LOGGER = Logger.getLogger(Server.class.getName());


	Database db;
	ServerSocket s;

	public Server(ServerSocket s) {
		this.s = s;
		this.db = new Database();
	}


	@Override
	public void run() throws RuntimeException {

		try {
			while(true) 
			{
				Socket c = s.accept();
				InputStream is = c.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				OutputStream os = c.getOutputStream();
				try {

					StringBuilder sb = new StringBuilder();
					String line;
					line = br.readLine();
					sb.append(line);
					LOGGER.info("User cmd: " + line);
					executeCommand(sb.toString(), os);
					os.flush();
				} catch(InvalidParameterException e) {
					os.write("Invalid parameters\n".getBytes());
					os.flush();
					LOGGER.log(Level.WARNING, "ioexception", e);
				} catch(IOException e) {
					LOGGER.log(Level.WARNING, "ioexception", e);
				} catch(SQLException e) {
					LOGGER.log(Level.WARNING, "sqlexception", e);
				} finally {
					c.close();
					LOGGER.info("Closed connection");
				}
			}
		} catch(Exception e) {
			LOGGER.log(Level.WARNING, "error in server", e);
			throw new RuntimeException(e);
		}

	}

	private void executeCommand(String cmdString, OutputStream os) throws IOException, SQLException {
		Command cmd = new Command(cmdString);
		LOGGER.info(cmd.toString());
		switch(cmd.getVerb()) 
		{
		case GET :
			executeGet(cmd, os);
			break;
		case GETALL:
			executeGetall(os);
			break;
		case SET:
			executeSet(cmd, os);
			break;
		default:
			throw new RuntimeException("unrecognized command: " + cmd.toString());
		}
	}

	private void executeGet(Command cmd, OutputStream os) throws IOException, SQLException {
		String key = cmd.getArgs().get(Command.ARG_KEYS.KEY.toString());

		String value = db.getValue(key);
		LOGGER.info("GOT VALUE " + value + " FROM DB");
		String outputstr = value + '\n';
		os.write(outputstr.getBytes());
		os.flush();
		return;
	}

	private void executeGetall(OutputStream os) throws IOException, SQLException {
		Map<String,String> kvs = db.getAllValues();
		for(Entry<String, String> kv : kvs.entrySet()) {
			os.write(kv.getKey().getBytes());
			os.write(',');
			os.write(kv.getValue().getBytes());
			os.write('\n');
		}
		os.flush();
	}

	private void executeSet(Command cmd, OutputStream os) throws IOException, SQLException {
		String key = cmd.getArgs().get(Command.ARG_KEYS.KEY.toString());
		String value = cmd.getArgs().get(Command.ARG_KEYS.VALUE.toString());
		LOGGER.info("Setting key " + key + " to value " + value);
		db.setValue(key, value);
	}

}
