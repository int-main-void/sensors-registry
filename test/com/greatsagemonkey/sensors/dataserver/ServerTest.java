package com.greatsagemonkey.sensors.dataserver;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ServerTest {

	private static final String hostname = "localhost";
	private static final int port = 1976;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRun() throws Exception {
		
		
		BufferedReader inFromUser = new BufferedReader( new InputStreamReader(System.in));
		Socket clientSocket = new Socket(hostname, port);
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

		String userCmd = inFromUser.readLine();
		outToServer.writeBytes(userCmd + '\n');
		
		System.out.println("FROM SERVER: ");
		String line;
		while( ( line = inFromServer.readLine()) != null) {
			System.out.println(line);
		}
		clientSocket.close();

	}

}
