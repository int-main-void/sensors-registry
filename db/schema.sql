CREATE TABLE registry 
(
  id    INT   NOT NULL,
  key   TEXT  NOT NULL, 
  value TEXT  NOT NULL,
  PRIMARY KEY(id)
);
  