package com.greatsagemonkey.sensors.dataserver;

import java.security.InvalidParameterException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;


public class Command {

	private final static Logger LOGGER = Logger.getLogger(Command.class.getName());
	
	public static enum VERBS { GET, GETALL, SET }

	public static enum ARG_KEYS { KEY, VALUE }
	
	private VERBS verb;
	private Map<String,String> args = new HashMap<String,String>();
	
	public VERBS getVerb() { return this.verb; }
	public Map<String,String> getArgs() { return Collections.unmodifiableMap(this.args); }
	
	public Command(VERBS verb, String key, String value) {
		if(verb == null || key == null || value == null) {
			throw new InvalidParameterException("invalid args for command");
		}
		this.verb = verb;
		args.put(ARG_KEYS.KEY.toString(), key);
		args.put(ARG_KEYS.VALUE.toString(), value);
	}
		
	public Command(String cmd)
	{
		String verbStr = cmd;
		String argStr = "";
		int idx = cmd.indexOf('?');
		if(idx > 0) {
			verbStr = cmd.substring(0,  idx);
			if(idx < cmd.length() - 1) {
				argStr = cmd.substring(idx+1, cmd.length());
			}
		}

		if(verbStr.equals(VERBS.GET.name())) {		
			this.verb = VERBS.GET;
			HashMap<String,String> kvs = extractKeyValuePairs(argStr);
			String keyname = kvs.get(ARG_KEYS.KEY.toString());
			if(keyname == null) {
				throw new InvalidParameterException("missing key for get command");
			}
			args.put(ARG_KEYS.KEY.toString(), keyname);
	
		} else if(verbStr.equals(VERBS.GETALL.name())) {
			this.verb = VERBS.GETALL;
			//ignore any args
		} else if(verbStr.equals(VERBS.SET.name())) {	
			this.verb = VERBS.SET;
			HashMap<String,String> kvps = extractKeyValuePairs(argStr);
			String key = kvps.get("KEY");
			if(key == null) {
				throw new InvalidParameterException("missing key for set command");
			}
			String value = kvps.get("VALUE");
			if(value == null) {
				throw new InvalidParameterException("missing value for get command");
			}
			args.put(ARG_KEYS.KEY.toString(), key);
			args.put(ARG_KEYS.VALUE.toString(), value);
		} else {
			throw new InvalidParameterException("unrecognized verb: " + verbStr);
		}
	}

	private HashMap<String,String> extractKeyValuePairs(String argStr) {
		HashMap<String,String> cmdArgs = new HashMap<String,String>();
		if(argStr.equals("")) { return cmdArgs; }
		
		String[] pairs = argStr.split(",");
		LOGGER.info("argStr: " + argStr);
		for(String kvp : pairs) {
			if(kvp.equals("")) { continue; }
			LOGGER.info("KVP: " + kvp);
			String[] kv = kvp.split("=");
			if(kv.length != 2) { throw new InvalidParameterException("invalid key/value pair: " + kvp); }
			String key = kv[0];
			String val = kv[1];
			cmdArgs.put(key, val);
		}
		return cmdArgs;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.verb.toString());
		sb.append('?');
		String comma = "";
		for(Entry<String,String> kvp : args.entrySet()) {
			sb.append(comma).append(kvp.getKey()).append('=').append(kvp.getValue());
			comma=",";
		}
		sb.append('\n');
		return sb.toString();
	}
}
